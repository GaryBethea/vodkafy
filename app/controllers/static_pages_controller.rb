class StaticPagesController < ApplicationController
  def home
  end

  def sovietunion
  end

  def help
  end

  def partners
  end

  def shop
  end
end
