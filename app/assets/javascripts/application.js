// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require_tree .
//= require bootstrap

$(function () {
  $('#get-help').on("click", function (event) {
    if ($("#help-email").val() === "") {
      $("div.no-email").show();
    } else {
      $("div.no-email").hide();
    } if ($("#help-problem").val() === "") {
      $("div.no-story").show();
    } else {
      $("div.no-story").hide();
    }
  });

  $('.vodka-info a').tooltip();
});

