NewAuthDemo::Application.routes.draw do
  resources :users, :only => [:create, :new, :show]
  resource :session, :only => [:create, :destroy, :new]

  get "/sovietunion" => 'static_pages#sovietunion'
  get "/help" => 'static_pages#help'
  get "/partners" => 'static_pages#partners'
  get "/shop" => 'static_pages#shop'
  get "/home" => 'static_pages#home'

  root :to => "users#show"
end
